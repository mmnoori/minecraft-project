const express = require('express')
const fs = require('fs')
const router = express.Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const moment = require('moment-timezone')


const auth = require('../auth/middleware')
const consts = require('../utils/consts')
const config = require('../config/config')
const errHandler = require('../utils/errHandler')
const fileManager = require('../tools/fileManager')
const multipleHandler = require('../config/multipleHandler')


const path = require('path')
const crypto = require('crypto') // crypto is a core node js module




const adminHandler = require('../utils/adminHandler')
const foodHandler = require('../utils/foodHandler')
const customerHandler = require('../utils/customerHandler')
const orderHandler = require('../utils/orderHandler')
const discountHandler = require('../utils/discountHandler')
const eventHandler = require('../utils/eventHandler')
const reportHandler = require('../utils/reportHandler')



// authenticate process
router.post(consts.ROUTE_AUTHENTICATE, (req, res) => {

    const Admin = multipleHandler.getModel(consts.ADMIN, multipleHandler.getFlag(req))

    const { username, password } = req.body

    Admin.findOne({ username }, (err, admin) => {

        if (err) errHandler(err)
        else if (!admin) res.status(consts.UNAUTHORIZED_CODE).json({ error: consts.INCORRECT_USER })
        else {

            const host = multipleHandler.getHost(req)

            // Match password
            // bcrypt.compare(password, user.password, (err, isMatch) => {
            //
            //     if (err) throw err
            //     if (isMatch) {
            //
            //         // Issue token
            //         const payload = { username: username }
            //         const token = jwt.sign(payload, config.adminJwtSecret, { expiresIn: '8h' })
            //
            //         try {
            //             res.cookie(consts.ADMIN_TOKEN, token, { httpOnly: true }).sendStatus(200)
            //
            //         } catch (e) {
            //             config.log(e)
            //         }
            //
            //     } else {
            //         res.status(consts.UNAUTHORIZED_CODE).json({ error: consts.INCORRECT_PASS })
            //     }
            // })

            if (password === admin.password) {

                // Issue token
                const payload = { username }
                const token = jwt.sign(payload, host.jwtSecret, { expiresIn: '8h' })

                try {
                    res.cookie(host.adminToken, token, { httpOnly: true }).sendStatus(consts.SUCCESS_CODE)

                } catch (e) {
                    config.log(e)
                }

            } else {
                res.status(consts.UNAUTHORIZED_CODE).json({ error: consts.INCORRECT_PASS })
            }
        }
    })
})



// TODO: after finishing project may work on executing queries synchronously to get better performance

router.post(consts.ROUTE_CHECK_TOKEN, auth.adminAuth, function(req, res) { res.send('OK') })

router.post(consts.ROUTE_CHANGE_PASS, auth.adminAuth, adminHandler.changePassword)
router.post(consts.ROUTE_LOGOUT, adminHandler.logOut)

router.post(consts.ROUTE_REGISTER_PUSH, auth.adminAuth, adminHandler.subscribe)
router.post(consts.ROUTE_GET_STATICS, auth.adminAuth, adminHandler.getStatics)

router.post(consts.ROUTE_GET_REPORTS, auth.adminAuth, reportHandler.getReports)

router.post(consts.ROUTE_MANAGEMENT, auth.adminAuth, adminHandler.getManagementDetails)
router.post(consts.ROUTE_UPDATE_MANAGEMENT, auth.adminAuth, adminHandler.updateManagement)

router.post(consts.ROUTE_PAY_PANEL, auth.adminAuth, adminHandler.createPayment)


router.post(consts.ROUTE_GET_CUSTOMERS, auth.adminAuth, customerHandler.getCustomers)
router.post(consts.ROUTE_GET_CUSTOMER_INFO, auth.adminAuth, customerHandler.getInfo)
router.post(consts.ROUTE_BAN_CUSTOMER, auth.adminAuth, customerHandler.banCustomer)

router.post(consts.ROUTE_NEW_DISCOUNT_CODE, auth.adminAuth, discountHandler.newDiscountCode)

router.post(consts.ROUTE_GET_ORDERS, auth.adminAuth, orderHandler.getOrders)

router.post(consts.ROUTE_GET_COMMENTS, auth.adminAuth, orderHandler.getComments)

// must use upload to handle multipart requests, body-parser does not support multipart bodies
// handling add / edit food in update food
router.post(consts.ROUTE_UPDATE_FOOD, auth.adminAuth, upload.single('img'), foodHandler.updateFood)
router.post(consts.ROUTE_DELETE_FOOD, auth.adminAuth, foodHandler.deleteFood)

router.post(consts.ROUTE_FOOD_GROUP_DISCOUNT, auth.adminAuth, foodHandler.setGroupFoodDiscount)

// router.post(consts.ROUTE_ACCEPT_ORDER, auth.adminAuth, orderHandler.acceptOrder)
router.post(consts.ROUTE_UPDATE_MEALS, auth.adminAuth, foodHandler.updateMeals)

router.post(consts.ROUTE_ADD_FOOD_LABEL, auth.adminAuth, foodHandler.addFoodLabel)
router.post(consts.ROUTE_EDIT_FOOD_LABEL, auth.adminAuth, foodHandler.editFoodLabel)
router.post(consts.ROUTE_DELETE_FOOD_LABEL, auth.adminAuth, foodHandler.deleteFoodLabel)


router.post(consts.ROUTE_HANDLE_ORDER, auth.adminAuth, orderHandler.handleOrder)



// handling add / edit food in update event
router.post(consts.ROUTE_UPDATE_EVENT, auth.adminAuth, upload.single('img'), eventHandler.updateEvent)
router.post(consts.ROUTE_DELETE_EVENT, auth.adminAuth, eventHandler.deleteEvent)









/*
* we call this funciton manually with 0 latency and it calls itself with calculated latency next times
* */
// config.checkMeal = (latency, argAdmin, callBack, flagParam) => {
//
//     config.checkMealTimeout = setTimeout(async () => {
//
//         const liveTime = moment().tz("Asia/Tehran").format('HH:mm:ss')
//
//         const liveHour = liveTime.substr(0,2)
//         const liveMinute = liveTime.substr(3,2)
//         const liveSecond = liveTime.substr(6,2)
//
//         let liveDate = `${liveHour}:${liveMinute}:${liveSecond}`
//
//         const Admin = multipleHandler.getModel(consts.ADMIN, flagParam)
//
//         // only execute query if no admin object was passed
//         let admin
//         if (!argAdmin) admin = await Admin.findOne({})
//         else admin = argAdmin
//
//         if (admin.acceptOrder === consts.OFF)
//             return (callBack)? callBack(admin): '' // if acceptOrder was permanently off, return and call callback if is given
//
//
//         let startLunch = `${admin.meals.lunch.startHour}:${admin.meals.lunch.startMin}:00`
//         let endLunch = `${admin.meals.lunch.endHour}:${admin.meals.lunch.endMin}:00`
//
//         let startDinner = `${admin.meals.dinner.startHour}:${admin.meals.dinner.startMin}:00`
//         let endDinner = `${admin.meals.dinner.endHour}:${admin.meals.dinner.endMin}:00`
//
//
//         let isInMidNightDinnerTime = false
//         const isDinnerEndAfterMidNight = admin.meals.dinner.endHour < admin.meals.dinner.startHour
//
//
//         // تحت این شرط هایی که گذاشتیم ساعات ناهار و شام به هیچ وجه نباید توی هم دیگه وارد بشه
//
//         // scenario that dinner ends after midnight, like 00:30
//         if (isDinnerEndAfterMidNight && (startDinner < liveDate || endDinner > liveDate)) isInMidNightDinnerTime = true
//
//
//
//         // in the format HH:MM:SS you can do a direct string comparison in JS for fuck's sake
//         if (((liveDate >= startLunch) && (liveDate < endLunch)) || // in lunch time
//             ((liveDate >= startDinner) && (liveDate < endDinner)) || // in normal dinner time
//             isInMidNightDinnerTime)  // in midNight dinner time
//         {
//
//             if (admin.acceptOrder !== consts.TRUE) { // if was not already TRUE
//                 admin.acceptOrder = consts.TRUE
//                 config.log('Changed acceptOrder = true')
//                 await admin.save().catch(err => config.log(err))
//             }
//         } else {
//
//             if (admin.acceptOrder !== consts.FALSE) { // if was not already FALSE
//                 admin.acceptOrder = consts.FALSE
//                 config.log('Changed acceptOrder = false')
//                 await admin.save().catch(err => config.log(err))
//             }
//         }
//
//         if (callBack) callBack(admin)
//
//         config.log('\n')
//         config.log(liveDate)
//         config.log(startLunch)
//         config.log(endLunch)
//         config.log(startDinner)
//         config.log(endDinner)
//
//
//         liveDate = new Date().setHours(liveHour, liveMinute, liveSecond)
//         // liveDate = new Date().setHours(21, 30, 0)
//
//         startLunch = new Date().setHours(admin.meals.lunch.startHour, admin.meals.lunch.startMin, 0)
//         endLunch = new Date().setHours(admin.meals.lunch.endHour, admin.meals.lunch.endMin, 0)
//
//         startDinner = new Date().setHours(admin.meals.dinner.startHour, admin.meals.dinner.startMin, 0)
//         endDinner = new Date().setHours(admin.meals.dinner.endHour, admin.meals.dinner.endMin, 0)
//
//
//         let remainingTime
//
//
//
//         if (isInMidNightDinnerTime) {
//
//             // live time is after 24
//             if (endDinner > liveDate) remainingTime = endDinner - liveDate
//
//             // live time is before 24
//             else if (endDinner < liveDate) remainingTime = endDinner+(1000*60*60*24) - liveDate
//
//             config.log('Remaining time to endMidNightDinner')
//
//         } else if (startLunch > liveDate){
//             remainingTime = startLunch - liveDate
//             config.log('Remaining time to startLunch')
//
//         } else if (endLunch > liveDate) {
//             remainingTime = endLunch - liveDate
//             config.log('Remaining time to endLunch')
//
//         } else if (startDinner > liveDate) {
//             remainingTime = startDinner - liveDate
//             config.log('Remaining time to startDinner')
//
//         } else if (endDinner > liveDate) {
//             remainingTime = endDinner - liveDate
//             config.log('Remaining time to endDinner')
//
//         } else { // live time is greater than any other meal time and we are not in midNightDinner time
//
//             remainingTime = startLunch+(1000*60*60*24) - liveDate
//             config.log('Remaining time to tommorow startLunch')
//         }
//
//         config.log('Remaining Time In Miliseconds: ')
//         config.log(remainingTime)
//
//         // it may happen that live time in milliseconds is exactly one of the meal times
//         // so to be able to compare remaining time properly we run function with 2 seconds delay
//         config.checkMeal(remainingTime+2000, null, null, flagParam)
//
//         config.log('\n')
//
//     }, latency)
// }

// اصن این چک کردن هر 10 دقیقه جدا از چک کردن وضعیت تراکنش برای هندل کردن پرداخت های موفق ناقص
// برای برگرداندن اعتبار کاربر بعد انقضا یک تراکنش نیاز هست
// ولی باز با آقای پرداخت باید هر 30 ثانیه چک کنیم
// چون یا باید بگیم پول بعدا برمیگرده دوباره سفارش بده یا نمیتونیم 10 دقیقه واستا اگر سفارشت شروع نشد دوباره سفارش بده
// ولی برای برگرداندن اعتبار هر 10 دقیقه
config.runTransactionChecker = () => setInterval(async () => {

    // گند کاری آفای پرداخت اینطوری باید جمع کرد

    // const transactions = await Transaction.find({state: consts.NOT_VERIFIED})
    //
    // if (transactions.length === 0) return config.log('There are no not verified transactions :)')
    //
    //
    // for (const transaction of transactions) {
    //
    //     const transid = transaction.transid
    //
    //     const data = await config.postToPhp(config.AQAYE_PARDAKHT_VERIFY_TRANSACTION, {
    //         amount: transaction.amount, transid, pin: config.AQAYE_PARDAKHT_PIN
    //     })
    //
    //     if (!data) {
    //         config.log('AP did NOT respond')
    //         continue
    //     }
    //
    //     // if response length is less that 3, it is an error code
    //     if (data < 0) {
    //         config.log('Error from AP: ' + data)
    //         continue
    //     }
    //
    //     const passedSeconds = Math.floor((new Date().getTime() - transaction.created) / 1000)
    //     config.log(transid + ' Passed Seconds : ' + passedSeconds)
    //     config.log(data)
    //
    //
    //     let order = await Order.findOne({transid})
    //
    //
    //     if (data == 1) { // payed
    //
    //         config.log('Transaction is payed : ' + transid)
    //
    //
    //
    //         order.state = consts.COMMITTED
    //         transaction.state = consts.PAYED
    //
    //         await order.save().catch(err => {
    //             if (err) config.log(err)
    //         })
    //
    //         await transaction.save().catch(err => {
    //             if (err) config.log(err)
    //         })
    //
    //
    //     }  else if (data == 0 && passedSeconds  > 10*60){ // NOT PAYED AND seconds passed > 600 = 10min
    //
    //         await orderHandler.handleOrderCancel(order, transid)
    //     }
    //
    //
    // }

}, 60*1000)



config.updateStaticMapImg = async (flagParam) => {

    const Admin = multipleHandler.getModel(consts.ADMIN, flagParam)

    const admin = await Admin.findOne()

    // let {statusCode, data} = await config.getRequest( `https://api.neshan.org/v2/static?key=`
    //     +`${config.NESHAN_API_KEY}&type=${'dreamy-gold'}&zoom=${'14'}&center=${admin.location[0]},${admin.location[1]}&width=300&height=300&marker=blue`)
    // console.log(config.getRequest('asdasd', {}))

    // config.log(admin.location)

    const {data, statusCode} = await config.getRequest(`https://map.ir/static?width=400&height=250&zoom_level=15&markers=color%3A${'orange'}%7C${admin.location.lng}%2C${admin.location.lat}`,
        { headers: { 'x-api-key': config.MAPIR_API_KEY }, responseType: "arraybuffer" })


    if (statusCode === consts.SUCCESS_CODE) {

        fs.writeFile(multipleHandler.getAssetsPath(flagParam)+ 'map.png' , data, function(err) {

            if (err) return config.log(err)
            config.log("The map image saved!", flagParam)
        })
    }
}

config.checkStaticMap = (flagParam) => {

    config.checkFileExistence(multipleHandler.getAssetsPath(flagParam) + 'map.png', (err) => {

        config.log(`${multipleHandler.getAssetsPath(flagParam) + 'map.png'} ${err ? 'does NOT exist' : 'exists'}`, flagParam)

        if (err) config.updateStaticMapImg(flagParam)
    })
}


module.exports = router
