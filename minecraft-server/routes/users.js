const express = require('express')
const router = express.Router()

const auth = require('../auth/middleware')
const consts = require('../utils/consts')


const tempStaticHandler = require('../utils/tempStaticHandler')
const worldHandler = require('../utils/worldHandler')
const userHandler = require('../utils/userHandler')




router.post(consts.ROUTE_INIT_WORLD, worldHandler.initServer)


// authenticate process
// router.post(consts.ROUTE_SEND_CODE, customerHandler.sendCode)
// router.post(consts.ROUTE_CHECK_CODE, customerHandler.checkCode)


// this route checks if cookie is valid and if it was, sends customer details from database
// router.post(consts.ROUTE_GET_CUSTOMER_INFO, staticsHandler.incrementView, auth.customerAuth, customerHandler.getInfo)
// router.post(consts.ROUTE_UPDATE_CUSTOMER_INFO, auth.customerAuth, customerHandler.updateInfo)

// router.post(consts.ROUTE_ADD_UPDATE_ADDRESS, auth.customerAuth, customerHandler.addUpdateAddress)
// router.post(consts.ROUTE_DELETE_ADDRESS, auth.customerAuth, customerHandler.deleteAddress)


// router.post(consts.ROUTE_GET_MESSAGES, auth.customerAuth, messageHandler.getMessages)
// router.post(consts.ROUTE_GET_ORDERS, auth.customerAuth, orderHandler.getOrders)



// router.post(consts.ROUTE_NEW_ORDER, auth.customerAuth, orderHandler.newOrder)
// router.post(consts.ROUTE_VALIDATE_DCODE, auth.customerAuth, discountHandler.checkDiscountCode)
// router.post(consts.ROUTE_SUBMIT_ORDER, auth.customerAuth, orderHandler.submitOrder)

// router.post(consts.ROUTE_SCORE_ORDER, auth.customerAuth, orderHandler.scoreOrder)


router.post(consts.ROUTE_NEW_CUSTOMER, userHandler.newCustomer)
router.post(consts.ROUTE_LOGOUT, userHandler.logOut)







module.exports = router
