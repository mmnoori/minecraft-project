const express = require('express')
const router = express.Router()

// const auth = require('../auth/middleware')
const consts = require('../utils/consts')
const config = require('../config/config')
const tasks = require('../tools/tasks')
// const errHandler = require('../utils/errHandler')

// const adminHandler = require('../utils/adminHandler')
const worldHandler = require('../utils/worldHandler')


router.post(consts.ROUTE_INIT_WORLD, tasks.checkRunningTasks, worldHandler.initServer)
router.post(consts.ROUTE_START_WORLD, tasks.checkRunningTasks, worldHandler.runWorld)

router.post(consts.ROUTE_STOP_WORLD, (req, res, next) => {
    tasks.decrement(consts.WORLDS_RUNNING)
    next()
}, worldHandler.stopWorld)

router.post(consts.ROUTE_DELETE_WORLD, worldHandler.deleteWorld)


router.post(consts.ROUTE_RUNNING_TASKS, tasks.getTasks)


module.exports = router
