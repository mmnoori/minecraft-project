const shell = require('shelljs')
const express = require('express')
const { exec, spawn } = require("child_process")
const bodyParser = require('body-parser')


const config = require('./config/config')
const proper = require('./tools/proper')
const consts = require('./utils/consts')

const server = express()


// Body Parser MiddleWare
// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
server.use(bodyParser.json())

// docker run -e TYPE=SPIGOT -e VERSION=1.18.1 -e ONLINE_MODE=FALSE -e EULA=TRUE -p 25565:25565 --name mc itzg/minecraft-server


// shelljs vs nodejs chid_process(windows)
// mkdir -p is not supported in child_process
// cd in shelljs changes working dir permanently for next commands, child_process changes temporary for just current command
// shell.exit() closes this whole nodejs instance


// if (!shell.which('git')) {
//     shell.echo('Sorry, this script requires git')
//     shell.exit(1)
// }

// does not work (on windows)
// exec(`mkdir ${destinationDir}`, ['-p'], (error, stdout, stderr) => {
//     if (error) console.log(`error: ${error.message}`)
//     if (stderr) console.log(`stderr: ${stderr}`)
//     if (stdout) console.log(`stdout: ${stdout}`)
// })

// Test
// NOTE: cd changes working directory for all shelljs instances
// const shell1 = require('shelljs')
// const shell2 = require('shelljs')
//
// shell1.cd('../..')
// shell1.exec('pwd')
//
// shell2.cd('..')
// shell2.exec('pwd')
//
// shell1.exec('pwd')


// shell.exec('pwd', function(code, stdout, stderr) {
//     if (code) console.log('Exit code:', code)
//     if (stdout) console.log('Program output:', stdout)
//     if (stderr) console.log('Program stderr:', stderr)
// })


// must be able to edit difficulty and pvp and online-mode automatically

// every server address must be dynamic // must configure bind and nginx automatically for this
// or just add whitelist to be able to delete users later

// must be able to detect inactivity, if no players are in server stop server

// limit max world size to prevent a 2GB World folder

// save status in mongodb/redis somehow sync to prevent any requests

// Abilities
// Reset World
// Grave Datapack : active / deactive

// Restart World
// start (stop after 5 min of inactivity)
// stop manually

// FUTURE UPDATES TO BE ADDED:
// Delete World
// Create World


// NOTICE:
// This file EXTREMELY relies on the TERMINAL which program gets executed
// MUST USE GIT BASH ON WINDOWS


function getJavaCommand({
                            port = 4567,

                            // version = '1.16.5',
                            // version = '1.17.1',
                            // version = '1.18.1',
                            version = '1.19.2',

                            username = 'mmnoori',

                            worldName = `new`,
                            // worldName = `starwz`
                        }) {


    const JAR_FILES_FOLDER = 'server-jars/'
    const WORLDS_FOLDER = `${version}-worlds`

    let destinationDir = `../${WORLDS_FOLDER}/${username}/${worldName}`
    let EULA_TXT_PATH = destinationDir + consts.EULA_TXT
    let SERVER_PROPERTIES_PATH = destinationDir + consts.SERVER_PROPERTIES

    // path from inside world directory
    const vanillaJarDirFromWorld = `../../../${JAR_FILES_FOLDER}minecraft_server_${version}.jar`
    const spigotJarDirFromWorld = `../../../${JAR_FILES_FOLDER}spigot-${version}.jar`


    let directoryFromProject = `../${WORLDS_FOLDER}/${username}/${worldName}`
    let universeDirFromWorldSpigot = `./`

    // 512MB RAM is unusable for both vanilla and spigot, at least must be 1024MB (Which takes up to 1.5G in real)
    // server configuration files will be created where ever below command is executed, and server configs do not exist
    const minMemory = '512M'
    const maxMemory = '2560M'


    const vanillaJar =
        // `java -Xms${minMemory} -Xmx${maxMemory} -jar ${vanillaJarDirFromWorld} --port ${port} --nogui --universe ../${WORLDS_FOLDER}/${username}/${worldName} --world ${worldName}`
        `java -Xms${minMemory} -Xmx${maxMemory} -jar ${vanillaJarDirFromWorld} --port ${port} --nogui --universe .`


    // init minecraft server folder
    let runSpigotJar =
        `java -Xms${minMemory} -Xmx${maxMemory} -XX:+UseG1GC -jar ${spigotJarDirFromWorld} --port ${port} --nogui`

    // spigot creates 3 world folder so must set it in 1 level deeper worldName/worldName

    // run minecraft server
    let runMinecraftServer =
        // `java -Xms512M -Xmx1024M -XX:+UseG1GC -jar ${spigotJarDirFromWorld} --port ${port} --nogui --universe ../${WORLDS_FOLDER}/${username}/${worldName} --world ${worldName}`
        // `java -Xms512M -Xmx1024M -XX:+UseG1GC -jar ${spigotJarDirFromWorld} --port ${port} --nogui --universe ${universeDirFromWorldSpigot} --world ${worldName}`
        `java -Xms${minMemory} -Xmx${maxMemory} -XX:+UseG1GC -jar ${spigotJarDirFromWorld} --port ${port} --nogui --universe ${universeDirFromWorldSpigot}`


    console.log('cd : ')
    console.log(destinationDir)

    console.log('Spigot init: ')
    console.log(runSpigotJar)

    console.log('Spigot start: ')
    console.log(runMinecraftServer)

    console.log('Vanilla start: ')
    console.log(vanillaJar)
}

getJavaCommand({})

return


// initServer()
// runWorld()


// proper.changeOnlineMode(SERVER_PROPERTIES_PATH, consts.TRUE, () => {})


function handleCommandResult(error, stdout, stderr) {
    if (error) console.log(`error: ${error.message}`)
    if (stderr) console.log(`stderr: ${stderr}`)
    if (stdout) console.log(`stdout: ${stdout}`)
}



async function start() {

    await proper.initRedis()


// routes which do not need authentication
    const publicRoutes = require('./routes/public')
    server.use('/api', publicRoutes)


    // just for test :)
    server.get('/test', (req, res) => {

        console.log(req.headers)
        // res.header('Cache-Control', 'max-age=60, public');
        // res.header('expires', 'Mon, 02 Mar 2020 18:28:11 GMT');

        // res.removeHeader('ETag');
        // res.header('ETag', '');

        // res.send(req.headers);
        res.send('dsfsdfsdgerhgrujtwulergp[rnj[pbpuovcdpfiwretmhrwthg')
    })

    server.get('/', (req, res) => {
        res.send('Hello From Express')
    })


    const port = 4051

    server.listen(port, config.hostAddress, (err) => {
        if (err) throw err
        else config.log(`> Ready on http://localhost:${port}`)
    })
}

start()

