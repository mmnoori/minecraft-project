const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')
const fs = require('fs')

const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const consts = require('../utils/consts')

// Bring in Models
const Admin = require('../models/admin')
const Customer = require('../models/customer')
const Order = require('../models/order')
const Food = require('../models/food')
const FoodLabel = require('../models/foodLabel')

module.exports = {

    insertFakeAdmin: async (host) => {


        const Admin = multipleHandler.getModel(consts.ADMIN, host.name)

        let admin = new Admin({
            username: 'admin',
            password: 'admin'
        })

        let result = await Admin.findOne({username: admin.username})

        // if did not find -> result is null
        // insert fake admin if admin did not exist
        if (!result) {

            // const generateSalt = new Promise((resolve, reject) => {
            //     bcrypt.genSalt(10, (err, salt) => {
            //         resolve(salt);
            //     });
            // });
            //
            // const saltString = await generateSalt;
            //
            // const hashPassword = new Promise((resolve, reject) => {
            //     bcrypt.hash(admin.password, saltString, async (err, hash) => {
            //
            //         if (err) config.log(err);
            //         else admin.password = hash;
            //         resolve();
            //     });
            // });
            //
            // await hashPassword; // NOTICE: hashPassword is NOT a function, we can't say hashPassword()

            await admin.save().catch(err => config.log(err))

            config.log(`fake admin added`, host.name)
        }
    },

    insertFakeCustomers: async () => {

        let customersCount = await Customer.count({})

        if (customersCount >= 200)  // check customers count
            return

        for (let i = 0; i < 4000; i++) {

            let customer = new Customer({
                _id: new mongoose.Types.ObjectId(),
                code: getRandomCode(6),
                name: getFirstName() + ' ' + getLastName(),
                birth: {
                    day: getDay(),
                    month: getMonth(),
                },
                phone: getPhoneNumber(),
                credit: getCredit()
            })

            let issue = false

            let savedCustomer = await customer.save().catch(err => {
                issue = true;
                config.log(err);
            })

            /*
            if there was any error it would be logged automatically,
            and below codes won't be executed.
            but if you use .catch() to handle promise rejection, below codes will be executed.
            catch function will be only executed when we have an err , it means we can't handle success doing like
            if (!err) log('success')
            */

            if (issue) {
                config.log('there was an issue')
                return
            }

            /*
             or we could have said:
            if (!savedCustomer) { // check if savedCustomer was undefined
                config.log('there was an issue');
                return;
            }
             */

            config.log('Added customer')

            /*
            1. let asd = customer.save() , asd is instance of Promise and is object
            2. let asd = await customer.save() , asd is NOT instance of Promise but is object
            3. let asd = customer.save((err) => log....) , asd is NOT instance of Promise and is undefined

            so model.save and model.insertMany are promises only if you don't define a callback function as argument,
            the callback function actually acts like .then()
            */
        }

    },
    insertFakeInvitedCustomers: async () => {

        let issue = false

        let customersCount = await Customer.count({}).exec()

        let invitedsArray = []

        // check if main customers were already inserted
        // and inviteds have not been added before
        if (customersCount >= 200 && customersCount <= 300) {

            for (let i = 0; i < 400; i++) {

                // creating random int between 0 and customersCount - 1
                let min = Math.ceil(0)
                let max = Math.floor(customersCount - 1)
                let random = Math.floor(Math.random() * (max - min + 1)) + min

                /*
                    Queries are not promises but they work fine with async/await either we use callback function or not
                    like find() or find((err, doocs) => {}) make no issues in async/await usage.

                    if we use .exec() at the end of query it would return a promise,
                    without .exec() query does not return a promise
                    but in both conditions async/await works fine for fucks sake :)

                    If you need a fully-fledged promise, use the .exec() function.
                */
                let customer = await Customer.findOne().skip(random).exec()

                invitedsArray.push({
                    code: getRandomCode(6),
                    name: getFirstName() + ' ' + getLastName(),
                    phone: getPhoneNumber(),
                    home: getHomeNumber(),
                    credit: getCredit(),
                    inviter: customer._id
                })

                customer.invitedsCount++
                customer.inviteds.push(invitedsArray[i]._id)

                await customer.save().catch(err => {
                    config.log(err);
                    issue = true;
                })

                if (issue) return
                else config.log('updated inviter')
            }


            let inviteds = await Customer.insertMany(invitedsArray).catch(err => {
                config.log(err)
                issue = true
            })

            if (issue) return
            else config.log('saved inviteds')

        }

    },
    insertFakeOrders: async () => {

        let ordersCount = await Order.count({})
        await insertFakeOrders(ordersCount)

    },
    insertFakeFoodLabels: async () => {

        let foodLabelsCount = await FoodLabel.count({})

        let issue = false

        if (foodLabelsCount > 5) return

        let foodLabelArray = []

        foodLabelArray.push(
            {name: 'چلوکباب'},
            {name: 'چلوکباب (تک سیخ)'},
            {name: 'خورشت ها'},
            {name: 'دریایی'},
            {name: 'کباب ها'},
            {name: 'خوراک ها'},
            {name: 'مخلفات'},
            {name: 'نوشیدنی'}
        )

        let foodLbales = await FoodLabel.insertMany(foodLabelArray).catch(err => {
            config.log(err)
            issue = true
        })

        if (issue) return
        else config.log('saved foodLabels')
    },
    insertFakeFoods: async () => {

        /*
         if use callback function with model.count function async/await won't work fine
        */

        let count =  await Food.count({})

        if (count >= 5)
            return

        await insertFoods()
    },
    getRandomCode

}

function getDay() {
    let day = ""
    let firstDigit = "0123"
    let secondDigit = "123456789"

    for (let i = 0; i < 2; i++) {
        if (day.charAt(0) === '3') day += '0' // 30 days

        else if (i === 0) day += firstDigit.charAt(Math.floor(Math.random() * firstDigit.length)) // add first digit
        else day += secondDigit.charAt(Math.floor(Math.random() * secondDigit.length)) // add second digit
    }

    return day
}

function getMonth() {
    let month = ""

    let firstDigit = "01"
    let secondDigit = "123456789"

    let extraDigits = "012"


    for (let i = 0; i < 2; i++) {
        if (month.charAt(0) === '1') month += extraDigits.charAt(Math.floor(Math.random() * extraDigits.length)) // 12 month

        else if (i === 0) month += firstDigit.charAt(Math.floor(Math.random() * firstDigit.length))
        else month += secondDigit.charAt(Math.floor(Math.random() * secondDigit.length))
    }

    return month
}

function getRandomCode(length) {
    let code = ""
    let numbers = "0123456789"
    let nonZeroNums = "123456789"

    for (let i = 0; i < length; i++) {
        // create 6 digit number
        if (i === 0) code += nonZeroNums.charAt(Math.floor(Math.random() * nonZeroNums.length))
        else code += numbers.charAt(Math.floor(Math.random() * numbers.length))
    }

    return code
}

function getFirstName() {
    let text = ""

    let array = ["امیر", "حسن", "غلام", "زهرا", "پریا", "محمد", "علی", "فاطمه", "زیبا", "سید بیژن", "صبا" , "بابک", "شیرین", "سوگند", "فریبا", "حمید", "عطابک", "رویا"]

    text = array[(Math.floor(Math.random() * array.length))]

    return text
}

function getLastName () {
    let text = ""

    let array = ["حمزه", "اعتدادی", "مجتهدی", "زیبا کناری", "پورباقری", "نجفی", "کاسپور", "محمدی", "غلام زاده", "منتظری", "آزاده" , "قریبی", "جوینده", "حیدری",
        "یزدانی", "پاینده", "قنبری", "قربانی", "نوازنده"]

    text = array[(Math.floor(Math.random() * array.length))]

    return text
}

function getPhoneNumber () {

    let phone = "093"
    let numbers = "0123456789"

    for (let i = 0; i < 8; i++)
        phone += numbers.charAt(Math.floor(Math.random() * numbers.length))

    return phone
}

function getHomeNumber () {

    let phone = "013"
    let numbers = "0123456789"

    for (let i = 0; i < 8; i++)
        phone += numbers.charAt(Math.floor(Math.random() * numbers.length))

    return phone
}

function getScore() {

    let score
    let numbers = "12345"

    score = numbers.charAt(Math.floor(Math.random() * numbers.length))

    return score
}


function getCredit() {

    // getting random double
    let min = 0
    let max = 1000

    // getting random int
    min = Math.ceil(0)
    max = Math.floor(1000)
    return (Math.floor(Math.random() * (max - min + 1)) + min) * 100
}

function getOrderState() {
    let text = ""

    let array = [consts.REJECTED, consts.DELIVERED]

    text = array[(Math.floor(Math.random() * array.length))]

    return text
}

function getOrderFoods(number) {
    let foods = []
    let text = ""

    let array = ['قزل آلا', 'چلوجوجه ترش', 'چلوکوبیده', 'چلوکباب ترش', 'چلوکباب فیله', 'نوشابه قوطی', 'ماست بورانی', 'زیتون پرورده']

    let min = 1
    let max = 3

    for (let i = 0; i < number; i++) {
        let tempFood = {
            name: array[(Math.floor(Math.random() * array.length))],
            count: Math.floor(Math.random() * (max - min + 1)) + min,
            price: Math.floor(Math.random() * (100000 - 20000 + 1)) + 20000
        }
        foods.push(tempFood)
    }

    return foods
}

function getFPrice() {

    // getting random int
    let min = Math.ceil(300)
    let max = Math.floor(1200)
    return (Math.floor(Math.random() * (max - min + 1)) + min) * 100
}

async function insertFakeOrders(ordersCount) {

    if (ordersCount <= 90) { // check if orders have not been added before

        let customerCount = await Customer.count({}).exec()

        // if (customerCount <= 400) // check if main customers and invited customers are added
        //     return;

        for (let i = 0; i < 305; i++) {

            // creating random int between 0 and customerCount - 1
            const min = Math.ceil(0)
            const max = Math.floor(customerCount - 1)
            const random = Math.floor(Math.random() * (max - min + 1)) + min

            let customer = await Customer.findOne().skip(random)

            const fPrice = getFPrice()

            let order = new Order({
                _id: new mongoose.Types.ObjectId(),
                owner: customer._id,
                phone: customer.phone,
                code: getRandomCode(6),
                state: getOrderState(),
                foods: getOrderFoods(5),
                address: 'گلسار ، بلوار دیلمان ، خیابان 198 ، پلاک 245 ، واحد 4',
                price: fPrice,
                payable: fPrice - 9000,
                score: {
                    food: getScore(),
                    courier: getScore()
                },
            })

            await order.save()

            // if below codes are executing, it means order saved successfully and promise did not throw rejection
            // cause we didn't used .catch() to handle it
            config.log('order saved')

            customer.orders.push(order._id)
            customer.ordersCount++

            await customer.save()
        }
    }
}

async function insertFoods() {

   /* let food;

    food = new Food({
        _id: new mongoose.Types.ObjectId(),
        name: 'پیتزا مخصوص',
        des: 'گوشت چرخ کرده تنوری، سالامی، پنیر، ژامبون گوشت و مرغ، سس مخصوص، قارچ',
        meal: 'شام',
        label: 'پیتزا',
        available: true,
        suggested: true,
        price: '31000',
        discount: '5',
        img: fs.readFileSync('D:\\Programming\\My Projects\\restaurant-server\\assets\\pizza.jpg'),
    });

    await food.save();
*/
    // config.log('food saved')

}



