const fs = require('fs')
const { exec, spawn } = require("child_process")
const { createClient } = require('redis')

const consts = require("../utils/consts")
const config = require("../config/config")

let redisClient


module.exports.initRedis = async () => {

    redisClient = createClient({ url: `redis://${consts.REDIS_HOST}:${consts.REDIS_PORT}` })

    redisClient.on('error', (err) => console.log('Redis Client Error', err))

    config.log('Connecting to Redis ...')
    await redisClient.connect()

    config.log('Resetting Redis ...')
    // Reset redis data
    await redisClient.set(consts.WORLDS_RUNNING, 0)
    await redisClient.set(consts.WORLDS_INITING, 0)

    let value = await redisClient.get(consts.WORLDS_RUNNING)

    value = parseInt(value) + 1


    // console.log('value')
    // console.log(value)
    // await redisClient.disconnect()
    // console.log('disconnected')

    console.log('finished initializing..........................')

}
module.exports.getRedisClient = () => {
    return redisClient
}


module.exports.agreeEulaTxt = function (pathToFile, callback) {

    replaceStringInFile(pathToFile, 'false', 'true', () => {
        callback()
    })
}

// Editing server.properties Functions (NEEDS RESTART)
module.exports.changeServerProperties = function (pathToFile, callback) {

    getFileData(pathToFile, (data) => {

        let lines = data.split('\n')

        console.log('lines before change: ')
        console.log(lines)

        for (let i = 0; i < lines.length; i++) {
            // For cracked minecraft
            if (lines[i].includes(consts.ONLINE_MODE)) {
                lines[i] = consts.ONLINE_MODE + 'false' + '\r'
                console.log(`changed ${consts.ONLINE_MODE} value\n\n`)
                // break

            // change difficulty
            } else if (lines[i].includes(consts.DIFFICULTY)) {
                lines[i] = consts.DIFFICULTY + 'hard' + '\r'
                console.log(`changed ${consts.DIFFICULTY} value\n\n`)
                // break
            }
        }

        console.log('lines after change: ')
        console.log(lines)

        const newData = stringArrayToServerProperties(lines)

        console.log('\n\n\ndata: ')
        console.log(newData)
        console.log('\n\n')

        saveFile(pathToFile, newData, () => {
            if (callback)
                callback()
        })

    })

}

// For Activating HUD Coordinates
// /trigger ch_toggle add 1
module.exports.copyDatapacks = function (from, to) {

    const command = `cp ${from} ${to}`

    exec(command, (error, stdout, stderr) => {

        if (error) console.log(`error: ${error.message}`)
        if (stderr) console.log(`stderr: ${stderr}`)
        if (stdout) console.log(`stdout: ${stdout}`)

        console.log('DataPacks are Copied !!')
    })
}


// we can change difficulty with server command but it won't change server.properties
// after server restart, server starts with server.properties configs

module.exports.changeDifficulty = function (pathToFile, callback) {
}
module.exports.changePvp = function (pathToFile, callback) {

}
module.exports.changeGameMode = function (pathToFile, callback) {

}
module.exports.changeMaxPlayers = function (pathToFile, callback) {

}
module.exports.changeMaxWorldSize = function (pathToFile, callback) {

}

// Server Commands (Sever must be running) (Effect is temporary till server is running)
// setOp, changeDifficulty, changeGameMode ... all changes are temporary, next time server runs with configuration files
module.exports.setOP = function () {}

module.exports.getOnlinePlayers = () => {}

// print working directory
module.exports.pwd = function () {

    console.log('\n\n')

    const runCmd = spawn('pwd')
    runCmd.stdout.on("data", data => {
        console.log(`stdout: ${data}`)
    })

    // exec(`pwd`, (error, stdout, stderr) => {
    //     if (error) console.log(`error: ${error.message}`)
    //     if (stderr) console.log(`stderr: ${stderr}`)
    //     if (stdout) console.log(`stdout: ${stdout}`)
    // })
    console.log('\n\n\n')

}


function stringArrayToServerProperties(array) {
    let string = ''
    // console.log('\n\n\n')
    for (let i = 0; i < array.length; i++) {
        // console.log(array[i])
        // console.log(config.toType(array[i]))
        // string = string + array[i]
        string = string + array[i] + '\n'
    }
    // console.log('string : ')
    // console.log(string)
    return string
}

function getFileData(pathToFile, callback) {

    fs.readFile(pathToFile, 'utf-8', (err, data) => {
        if (err) throw err

        callback(data)
    })
}

function saveFile(pathToFile, data, callback) {
    fs.writeFile(pathToFile, data, 'utf-8', (err) => {
        if (err) throw err

        console.log(`${pathToFile} Changed successfully !`)
        if (callback) callback()
    })
}

function replaceStringInFile(pathToFile, originalString, newString, callback) {

    fs.readFile(pathToFile, 'utf-8', (err, data) => {
        if (err) throw err

        // replace String
        const newValue = data.replace(originalString, newString)

        // write new value
        fs.writeFile(pathToFile, newValue, 'utf-8', (err) => {
            if (err) throw err

            console.log(`${pathToFile} Changed successfully !`)
            if (callback) callback()
        })
    })
}


