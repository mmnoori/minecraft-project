const ftp = require("basic-ftp")
const { exec } = require("child_process")

const dConverter = require('../tools/dateConverter')

const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')

const FTP_HOST = 'noc.netmihan.com'
const FTP_PORT = 21
const FTP_USER = 'b289400'
const FTP_PASS = 'sv3bHFQDMg7'

const ZIP_FILE = `backup.tar.gz`

// NOTICE:
// This file EXTREMELY relies on the TERMINAL which program gets executed
// MUST USE GIT BASH ON WINDOWS

let commands = [
    `rm -rf temp`, // remove if exists
    `mkdir temp`,
    `mongodump --out temp/db`, // mongodump --out can create several subdirectories
    `cp config/logs.txt temp`,
]

// Dynamic host static files
multipleHandler.hosts.forEach((host, index) => {

    commands.push(
        `cd temp && mkdir ${host.name}`,
        // `mkdir -p temp/${host.name}/images`, // This works on normal git-bash CLI but not with node child_process
        `cp statics/${host.name}/images/* temp/${host.name}`,
    )
})
// create zip file
commands.push(`cd temp && tar czf ${ZIP_FILE} *`)

let removeJunksCommand = `rm -rf temp`


/*
* Execute shell commands (Recursive Function)
*/
function executeCommands(command, index) {

    exec(command, (error, stdout, stderr) => {

        // config.log('index = ' + index)

        handleCommandResult (error, stdout, stderr)

        index++

        // if there was more commands
        if (commands[index])
            executeCommands(commands[index], index)
        else
            sendViaFtp()

    })

}

function handleCommandResult (error, stdout, stderr) {

    if (error) config.log(`error: ${error.message}`)
    if (stderr) config.log(`stderr: ${stderr}`)
    if (stdout) config.log(`stdout: ${stdout}`)
}


async function sendViaFtp() {

    const client = new ftp.Client()

    // Logs out all communication with the FTP server.
    client.ftp.verbose = true

    config.log('Started FTP For Back Up ... ')

    try {
        await client.access({
            host: FTP_HOST,
            port: FTP_PORT,
            user: FTP_USER,
            password: FTP_PASS,
            // secure: true
        })

        // print list
        // config.log(await client.list())

        // 'jYYYY-jMM-jDD HH:mm'
        let [date, time] = dConverter.getLiveDate().split(' ')

        let directory

        if (config.isDevelopment) directory= 'backup-dev/' + date
        else directory= 'backup/' + date

        // this is able to create several subdirectories
        await client.ensureDir(directory)

        // replacing colon with unicode character U+A789 to keep uninterrupted file name on windows OS
        time = time.replace(':', '꞉')

        let zipNameInServer = `${time}.tar.gz`

        // uploading to directory
        // automatically changes file name for server
        await client.uploadFrom(`temp/${ZIP_FILE}`, zipNameInServer)

        config.log('Ended FTP For Back Up ... ')


        // Remove Junk Backup Files on Client
        exec(removeJunksCommand, handleCommandResult)

        // await client.downloadTo("README_COPY.md", "README_FTP.md")


    }
    catch(err) {
        config.log(err)
    }
    client.close()
}











async function backup() {

    setInterval(() => {

        config.log(`TIME FOR BACK UP :`)

        executeCommands(commands[0], 0)


    }, 60*60*1000)
    // }, 1*60*1000)
    // }, 30*1000)

    // if (config.isDevelopment) return




}




module.exports = backup

