const config = require('../config/config')
const proper = require('../tools/proper')
const consts = require('../utils/consts')

const Exported = {
    checkRunningTasks: async (req, res, next) => {

        let { worldsRunning, worldsIniting } = await Exported.getTasks()

        console.log(`worldsRunning: ${worldsRunning}`)
        console.log(`worldsIniting: ${worldsIniting}`)


        // checking number of tasks
        if (worldsIniting <= consts.MAX_WORLDS_INITING && worldsRunning <= consts.MAX_WORLDS_RUNNING) {
            next()
        } else
            res.send('Server is BUSY')

    },
    getTasks: async () => {

        let worldsRunning, worldsIniting

        worldsIniting = await proper.getRedisClient().get(consts.WORLDS_INITING)
        worldsRunning = await proper.getRedisClient().get(consts.WORLDS_RUNNING)

        return { worldsRunning, worldsIniting }
    },

    increment: async (key) => {
        let value = await proper.getRedisClient().get(key)

        value = Number(value) + 1

        await proper.getRedisClient().set(key, value)
    },

    decrement: async (key) => {
        let value = await proper.getRedisClient().get(key)

        if (value > 0) {
            value = Number(value) - 1

            await proper.getRedisClient().set(key, value)
        }

    }
}


module.exports = Exported