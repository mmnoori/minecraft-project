/** Gregorian & Jalali (Hijri_Shamsi,Solar) Date Converter Functions
 Author: JDF.SCR.IR =>> Download Full Version : http://jdf.scr.ir/jdf
 License: GNU/LGPL _ Open Source & Free _ Version: 2.72 : [2017=1396]
 --------------------------------------------------------------------
 1461 = 365*4 + 4/4   &  146097 = 365*400 + 400/4 - 400/100 + 400/400
 12053 = 365*33 + 32/4    &    36524 = 365*100 + 100/4 - 100/100   */

const moment = require('moment-timezone')
const jmoment = require('moment-jalaali')

function gregorian_to_jalali(gy, gm, gd) {
    g_d_m = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    if (gy > 1600) {
        jy = 979;
        gy -= 1600;
    } else {
        jy = 0;
        gy -= 621;
    }
    gy2 = (gm > 2) ? (gy + 1) : gy;
    days = (365 * gy) + (parseInt((gy2 + 3) / 4)) - (parseInt((gy2 + 99) / 100)) + (parseInt((gy2 + 399) / 400)) - 80 + gd + g_d_m[gm - 1];
    jy += 33 * (parseInt(days / 12053));
    days %= 12053;
    jy += 4 * (parseInt(days / 1461));
    days %= 1461;
    if (days > 365) {
        jy += parseInt((days - 1) / 365);
        days = (days - 1) % 365;
    }
    jm = (days < 186) ? 1 + parseInt(days / 31) : 7 + parseInt((days - 186) / 30);
    jd = 1 + ((days < 186) ? (days % 31) : ((days - 186) % 30));
    // return [jy, jm, jd];

    if (jd / 10 < 1) jd = `0${jd}`; // adding zero if it was a single digit number
    if (jm / 10 < 1) jm = `0${jm}`;

     // for fuck sake mongoDB engine can sort by both - and /
    // return `${jy}-${jm}-${jd}`;
    return `${jy}-${jm}-${jd}`;
}

function getJalali(gy, gm, gd) {
    g_d_m = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    if (gy > 1600) {
        jy = 979;
        gy -= 1600;
    } else {
        jy = 0;
        gy -= 621;
    }
    gy2 = (gm > 2) ? (gy + 1) : gy;
    days = (365 * gy) + (parseInt((gy2 + 3) / 4)) - (parseInt((gy2 + 99) / 100)) + (parseInt((gy2 + 399) / 400)) - 80 + gd + g_d_m[gm - 1];
    jy += 33 * (parseInt(days / 12053));
    days %= 12053;
    jy += 4 * (parseInt(days / 1461));
    days %= 1461;
    if (days > 365) {
        jy += parseInt((days - 1) / 365);
        days = (days - 1) % 365;
    }
    jm = (days < 186) ? 1 + parseInt(days / 31) : 7 + parseInt((days - 186) / 30);
    jd = 1 + ((days < 186) ? (days % 31) : ((days - 186) % 30));
    return {year: jy, month: jm, day: jd}
}


function jalali_to_gregorian(jy, jm, jd) {
    if (jy > 979) {
        gy = 1600;
        jy -= 979;
    } else {
        gy = 621;
    }
    days = (365 * jy) + ((parseInt(jy / 33)) * 8) + (parseInt(((jy % 33) + 3) / 4)) + 78 + jd + ((jm < 7) ? (jm - 1) * 31 : ((jm - 7) * 30) + 186);
    gy += 400 * (parseInt(days / 146097));
    days %= 146097;
    if (days > 36524) {
        gy += 100 * (parseInt(--days / 36524));
        days %= 36524;
        if (days >= 365) days++;
    }
    gy += 4 * (parseInt(days / 1461));
    days %= 1461;
    if (days > 365) {
        gy += parseInt((days - 1) / 365);
        days = (days - 1) % 365;
    }
    gd = days + 1;
    sal_a = [0, 31, ((gy % 4 === 0 && gy % 100 !== 0) || (gy % 400 === 0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    for (gm = 0; gm < 13; gm++) {
        v = sal_a[gm];
        if (gd <= v) break;
        gd -= v;
    }
    return [gy, gm, gd]
}

// by default options we can only change time zone but not calendar in node js
// not even by changing process.env.TZ = 'Asia.Tehran'
// const nDate = new Date().toLocaleString('en-US', {
//     timeZone: 'Asia/Tehran'
// });


// console.log( moment().tz("2013-12-01", "America/Los_Angeles").daysInMonth()  )


// مشکل تبدیل تاریخ بین المللی UTC به شمسی اینه که زمان ایران +3:30 هست یعنی 3 ساعت و نیم در اول روز شمسی با تبدیل تاریخ گرینویچ به شمسی ما تاریخ روز قبل رو میگیریم
// از طرفی ما میخوایم برنامه مستقل از Local time روی هر سیستمی درست که تاریخ Iran/Tehran ست نشده هم درست کار بکنه

// let date = new Date();
// console.log(`${date.getUTCFullYear()}/${date.getUTCMonth()}/${date.getUTCDate()} ${date.getUTCHours()}:${date.getUTCMinutes()}`)






function getJalaliDate() {
    // const date = new Date()
    // return getJalali(date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate())

    const date = jmoment().format('jYYYY/jM/jD').split('/')
    return {year: date[0], month: date[1], day: date[2]}
}

function getTehranTime() {

    return moment().tz("Asia/Tehran").format('HH:mm:ss')
}

function getLiveDate() {

    // خود new Date تاریخ Universal Time رو میاره
    // ولی وقتی getDate میزنی، روز رو از تاریخ Local time سیستم میاره
    // باید از تابع های getUTC استفاده کنیم
    // const date = new Date()

    // return moment().tz("Asia/Tehran").format(
    //     gregorian_to_jalali(date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate())
    //     +
    //     ' HH:mm')


    const date = jmoment().format('jYYYY-jMM-jDD')
    return moment().tz("Asia/Tehran").format(date + ' HH:mm')
}

function getTodayForBirth() {

    // const date = new Date()
    // return moment().tz("Asia/Tehran").format( gregorian_to_jalali(date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate()) )

    const date = jmoment().format('jYYYY-jMM-jDD')
    return moment().tz("Asia/Tehran").format( date )
}

function getDaysInJalaliMonth(year, month) {

    // month starts from 0 to 11
    return jmoment.jDaysInMonth(year, month-1)
}

module.exports = {getLiveDate, getTodayForBirth, getJalaliDate, getDaysInJalaliMonth, getTehranTime}
