module.exports = {

    REDIS_HOST: process.env.REDIS_HOST,
    REDIS_PORT: process.env.REDIS_PORT,

    // REDIS_HOST: 'redis',
    // REDIS_PORT: '6379',


    EULA_TXT: '/eula.txt',
    SERVER_PROPERTIES: '/server.properties',

    NEEDS_EULA_AGREEMENT: 'You need to agree to the EULA in order to run the server. Go to eula.txt for more info.',
    SUCCESSFUL_STARTED: 'Done',

    ONLINE_PLAYERS: 'players online:',
    LOGGED_IN: `logged in with entity id`,

    ONLINE_MODE: 'online-mode=',
    DIFFICULTY: 'difficulty=',


    WORLDS_RUNNING: 'WORLDS_RUNNING',
    WORLDS_INITING: 'WORLDS_INITING',

    MAX_WORLDS_RUNNING: 1,
    MAX_WORLDS_INITING: 1,


    ERR: 'خطای داخلی',

    TRUE: 'true',
    FALSE: 'false',





    USER: 'User',
    WORLD: 'World',
    TEMP_STATIC: 'TempData',





    ANDROID: 'ANDROID',
    WEB: 'WEB',

    AVAILABLE: 'موجود',
    NOT_AVAILABLE: 'ناموجود',

    VALID: 'معتبر',
    BORN_TODAY: 'متولد امروز',
    BANNED: 'مسدود شده',



    OFF: 'OFF',
    ON: 'ON',

    ONLINE: 'ONLINE',
    CASH: 'CASH',


    PERMISSIONS: 'pr',


    DEFAULT: 'default',
    DENIED: 'denied',
    GRANTED: 'granted',

    NOTIFICATION: 'NOTIFICATION',
    A2HS: 'A2HS',



    INCORRECT_PASS: 'Incorrect password',
    INCORRECT_USER: 'Incorrect Username',



    ADMIN_TOKEN : 'at',
    CUSTOMER_TOKEN : 'ct',
    ANDROID_TOKEN : 'ant',



    ROUTE_INIT_WORLD: '/init-world',
    ROUTE_START_WORLD: '/start-world',
    ROUTE_STOP_WORLD: '/stop-world',
    ROUTE_DELETE_WORLD: '/delete-world',

    ROUTE_RUNNING_TASKS: '/running-tasks',



    ROUTE_AUTHENTICATE: '/authenticate',
    ROUTE_REGISTER_PUSH: '/register-push',
    ROUTE_GET_STATICS: '/get-statics',


    ROUTE_PAYMENT_CALLBACK: '/payment-callback',

    ROUTE_IDPAY_CALLBACK: '/idpay-callback',
    ROUTE_IDPAY_CUSTOMER_CALLBACK: '/payement',


    MONTHS: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],



    ERR_NOT_IN_ACCEPT_ORDER_TIME : 'ثبت سفارش در ساعات کاری مجموعه صورت میگیرد',
    ERR_ORDER_TEMPORARY_OFF : 'ثبت سفارش موقتا صورت نمی گیرد',




    SUCCESS_CODE: 200,
    GONE_CODE: 410,

    CREATED_CODE: 201,

    NO_CONTENT: 205,
    BAD_REQ_CODE: 400,
    UNAUTHORIZED_CODE: 401,
    NOT_FOUND_CODE: 404,
    INT_ERR_CODE: 500
}
