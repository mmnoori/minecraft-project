const { exec, spawn } = require("child_process")

const config = require('../config/config')
const proper = require('../tools/proper')
const tasks = require('../tools/tasks')
const consts = require('../utils/consts')


let runningMinecraftSpawn

const JAR_FILES_FOLDER = 'minecraft-server/assets/server-jars'

// 512MB RAM is unusable for both vanilla and spigot, at least must be 1024MB (Which takes up to 1.5G in real)
// server configuration files will be created where ever below command is executed, and server configs do not exist
const minMemory = '512M'
const maxMemory = '1024M'

function getWorldsFolder({ version }) {
    return `worlds/${version}-worlds`
}

function getDestinationFolder({ worldName, username, version }) {

    const WORLDS_FOLDER = getWorldsFolder({ version })

    return `../${WORLDS_FOLDER}/${username}/${worldName}`
}

function getEulaTxtPath(destinationDir) {
    return destinationDir + consts.EULA_TXT
}

function getServerPropertiesPath(destinationDir) {
    return destinationDir + consts.SERVER_PROPERTIES
}

function getVanillaJarDirectoryFromWorld(version) {
    return `../../../../${JAR_FILES_FOLDER}/minecraft_server_${version}.jar`
}

function getSpigotJarDirectoryFromWorld(version) {
    return `../../../../${JAR_FILES_FOLDER}/spigot-${version}.jar`
}

function getDatapacksPath(version) {
    return `./assets/datapacks/${version}/*`
}

function getDatapacksDestinationPath(destinationDir, worldName) {
    // for Spigot
    return `${destinationDir}/${worldName}/datapacks`
}


function getInitServerCommand({ worldName, username, version, port, build }) {


    const WORLDS_FOLDER = getWorldsFolder({ version })


    const destinationDir = getDestinationFolder({ worldName, username, version })
    const EULA_TXT_PATH = getEulaTxtPath(destinationDir)
    const SERVER_PROPERTIES_PATH = getServerPropertiesPath(destinationDir)

    // path from inside world directory
    const vanillaJarDirFromWorld = getVanillaJarDirectoryFromWorld(version)
    const spigotJarDirFromWorld = getSpigotJarDirectoryFromWorld(version)

    const vanillaJar =
        `java -Xms${minMemory} -Xmx${maxMemory} -jar ${vanillaJarDirFromWorld} --port ${port} --nogui --universe ../${WORLDS_FOLDER}/${username}/${worldName} --world ${worldName}`


    // init minecraft server folder
    let runSpigotJar =
        `java -Xms${minMemory} -Xmx${maxMemory} -XX:+UseG1GC -jar ${spigotJarDirFromWorld} --port ${port} --nogui`


    // Minecraft initial server files will be created where ever command is being executed,
    // so have to cd into the right directory before running command
    return `cd ${destinationDir} && ${runSpigotJar}`
}

function getRunServerCommand({ worldName, username, version, port, build }) {

    const spigotJarDirFromWorld = getSpigotJarDirectoryFromWorld(version)

    // spigot creates 3 world folder so must set it in 1 level deeper worldName/worldName
    return `java -Xms512M -Xmx1024M -XX:+UseG1GC -jar ${spigotJarDirFromWorld} --port ${port} --nogui --universe . --world ${worldName}`
}


const Exported = {
    getRunningTasks: async (req, res, next) => {

        let tasks = await tasks.getTasks()

        res.json(tasks)

    },
    initServer: function (req, res, next) {

        const { worldName, username, version, port, build } = req.body

        config.log(`initializing server ...`)

        config.log(`\n`)
        config.log(`worldName : ${worldName}`)
        config.log(`username : ${username}`)
        config.log(`version : ${version}`)
        config.log(`port : ${port}`)
        config.log(`\n`)

        if (worldName === undefined || username === undefined || version === undefined || port === undefined)
            return res.send(' BAD PARAMS')


        const destinationDir = getDestinationFolder({ worldName, username, version })
        const EULA_TXT_PATH = getEulaTxtPath(destinationDir)

        // Check if world already exists
        config.checkFileExistence(EULA_TXT_PATH, async (err) => {
            // file does not exist
            if (err) {

                await config.ensureDirectoryExistence(destinationDir)

                // this cd does not affect any other working directory while running
                // let command = `cd ${destinationDir} && ${runSpigotJar}`
                const command = getInitServerCommand({ worldName, username, version, port, build })

                exec(command, (error, stdout, stderr) => {

                    if (error) console.log(`error: ${error.message}`)
                    if (stderr) console.log(`stderr: ${stderr}`)
                    if (stdout) console.log(`stdout: ${stdout}`)

                    if (stdout.includes(consts.NEEDS_EULA_AGREEMENT))
                        proper.agreeEulaTxt(EULA_TXT_PATH, () => {

                            proper.changeServerProperties(getServerPropertiesPath(destinationDir), () => {

                            })
                        })
                })


            } else { // server is already initialized

                config.log(`server files are already created`)

            }
        })

    },
    runWorld: function (req, res, next) {

        // can't use <cd> with {spawn}=child_process, nor can use <&&>

        const { worldName, username, version, port, build } = req.body

        const destinationDir = getDestinationFolder({ worldName, username, version })

        const command = getRunServerCommand({ worldName, username, version, port, build })


        runningMinecraftSpawn = spawn(command, {
            shell: true, // able to use the shell syntax in the passed command, just like exec
            cwd: destinationDir, // this way directory only and only changes for current spawn
            detached: true
        })

        runningMinecraftSpawn.stdout.on("data", data => {
            console.log(`stdout: ${data}`)


            if (data.includes(consts.SUCCESSFUL_STARTED)) {
                console.log('\nserver has successfully started\n')


                const datapacksPath = getDatapacksPath(version)
                const to = getDatapacksDestinationPath(destinationDir, worldName)
                proper.copyDatapacks(datapacksPath, to)

                // setInterval(() => {
                //
                //     process.stdin.write('list')
                //     process.stdin.end()
                // }, 20*1000)

            } else if (data.includes(consts.ONLINE_PLAYERS)) {


            }

        })
        runningMinecraftSpawn.stderr.on("data", data => {
            console.log(`stderr: ${data}`)
        })
        runningMinecraftSpawn.on('error', (error) => {
            console.log(`error: ${error.message}`)
        })
        runningMinecraftSpawn.on("close", code => {
            console.log(`child process exited with code ${code}`)
        })


        // process.stdin.write(runMinecraftServer)

        // shell.cd(destinationDir) // this changes working directory
        //
        // let child = shell.exec(runSpigotJar, {async:true})
        //
        // child.stdout.on('data', (data) => {
        //
        //     // console.log(`stdout : `)
        //     // console.log(data)
        // })
        //
        // child.stderr.on("data", data => {
        //     console.log(`stderr: ${data}`)
        // })
        // child.on('error', (error) => {
        //     console.log(`error: ${error.message}`)
        // })
        // child.on("close", code => {
        //     // TODO: change status
        //     console.log(`child process exited with code ${code}`)
        // })

    },

    stopWorld: (req, res, next) => {

        // process.stdin.write(`stop`)


        process.kill(-runningMinecraftSpawn.pid)

        res.send('stopped')
    },

    deleteWorld: () => {

    }

}


module.exports = Exported



