const { exec, spawn } = require("child_process")

const config = require('../config/config')
const proper = require('../tools/proper')
const consts = require('../utils/consts')



module.exports = {
    initServer: function (req, res, next) {

        let {name, version} = req.body

        config.log(`initializing server ...`)

        config.checkFileExistence(EULA_TXT_PATH, async (err) => {
            // file does not exist
            if (err) {

                await config.ensureDirectoryExistence(destinationDir)

                // this cd does not affect any other working directory while running
                let command = `cd ${destinationDir} && ${runJar}`

                exec(command, (error, stdout, stderr) => {

                    if (error) console.log(`error: ${error.message}`)
                    if (stderr) console.log(`stderr: ${stderr}`)
                    if (stdout) console.log(`stdout: ${stdout}`)

                    if (stdout.includes(consts.NEEDS_EULA_AGREEMENT))
                        proper.agreeEulaTxt(EULA_TXT_PATH, () => { })
                })


            }
            else { // server is already initialized

                config.log(`server files are already created`)

            }
        })


        // Using ShellJs Because we could not cd in windows
        // -p: full path (and create intermediate directories, if necessary)
        // shell.mkdir('-p', destinationDir)
        // shell.cd(destinationDir) // this changes working directory
        //
        //
        // let child = shell.exec(runJar, {async:true})
        //
        // child.stdout.on('data', (data) => {
        //
        //     // console.log(`stdout : `)
        //     // console.log(data)
        //
        //     if (data.includes(consts.NEEDS_EULA_AGREEMENT)) {
        //         console.log('\nNeed To Edit EULA.TXT\n')
        //
        //         // SUPER IMPORTANT :
        //         // WHILE WE HAVE NOT CD BACK, ANY DYNAMIC PATH IN THIS NODE JS APP WILL BE WRONG
        //         // back to main project directory
        //         shell.cd(CWD)
        //
        //         proper.agreeEulaTxt(EULA_TXT_PATH, () => {
        //
        //         })
        //
        //     } else if (data.includes(consts.SUCCESSFUL_STARTED)) {
        //         console.log('\nserver has successfully started\n')
        //
        //     }
        // })
        //
        // child.stderr.on("data", data => {
        //     console.log(`stderr: ${data}`)
        // })
        //
        // child.on('error', (error) => {
        //     console.log(`error: ${error.message}`)
        // })
        //
        // child.on("close", code => {
        //     // TODO: change status
        //     console.log(`child process exited with code ${code}`)
        // })

    },
    runWorld: function(req, res, next) {

        // can use <cd> with {spawn}=child_process, nor can use <&&>

        let pingGoogle = 'ping google.com -t'

        // const runCmd = spawn('ping', ["google.com", '-t'])
        // const runCmd = spawn('ping google.com -t') // DOES NOT WORK THIS WAY
        // const runCmd = spawn('pwd')
        // const runCmd = spawn('pwd', ['&&', 'ping', 'google.com']) // DOES NOT WORK THIS WAY

        // const runCmd = spawn(pingGoogle, {
        const runCmd = spawn(runMinecraftServer, {
            shell: true, // able to use the shell syntax in the passed command, just like exec
            cwd: destinationDir // this way directory only and only changes for current spawn
        })

        runCmd.stdout.on("data", data => {
            console.log(`stdout: ${data}`)



            if (data.includes(consts.SUCCESSFUL_STARTED)) {
                console.log('\nserver has successfully started\n')


                // setInterval(() => {
                //
                //     runCmd.stdin.write('list')
                //     runCmd.stdin.end()
                // }, 20*1000)

            } else if (data.includes(consts.ONLINE_PLAYERS)) {


            }



        })
        runCmd.stderr.on("data", data => {
            console.log(`stderr: ${data}`)
        })
        runCmd.on('error', (error) => {
            console.log(`error: ${error.message}`)
        })
        runCmd.on("close", code => {
            console.log(`child process exited with code ${code}`)
        })


        // runCmd.stdin.write(runMinecraftServer)




        // shell.cd(destinationDir) // this changes working directory
        //
        // let child = shell.exec(runJar, {async:true})
        //
        // child.stdout.on('data', (data) => {
        //
        //     // console.log(`stdout : `)
        //     // console.log(data)
        // })
        //
        // child.stderr.on("data", data => {
        //     console.log(`stderr: ${data}`)
        // })
        // child.on('error', (error) => {
        //     console.log(`error: ${error.message}`)
        // })
        // child.on("close", code => {
        //     // TODO: change status
        //     console.log(`child process exited with code ${code}`)
        // })

    }


}



