// to avoid CWD issue, run this file from project node ./config/test.js

const express = require('express')

const config = require('../config/config')



const server = express()


let i = 0

async function randomResponse(req, res, next) {

    let index = i

    i++

    console.log(`request ${index} started`)

    let randomMilliSeconds = config.getRandomInteger(1, 9) * 1000

    console.log(randomMilliSeconds)

    await new Promise((resolve, reject) => {

        setTimeout(() => {

            res.send(`request ${index} Finished in ` + randomMilliSeconds)
            resolve()
        }, randomMilliSeconds)
    })

    console.log(`request ${index} Finished in ` + randomMilliSeconds)
}


// test express concurrent processing
server.get('/test', randomResponse)


const port = 7894

server.listen(port, config.hostAddress, (err) => {
    if (err) throw err
    else config.log(`> Ready on http://localhost:${port}`)
})