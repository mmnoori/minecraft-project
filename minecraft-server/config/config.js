const axios = require("axios")
const fs = require('fs')
const { exec, spawn } = require("child_process")
const moment = require('moment-timezone')

const dConverter = require('../tools/dateConverter')


// WebStorm / Windows Terminal : set NODE_ENV=development
// Git Bash / Linux Terminal : export NODE_ENV=development
const isDevelopment = (process.env.NODE_ENV === 'development')


// NOTE: Must be on 0.0.0.0 to reach requests with Docker
const hostAddress = '0.0.0.0'
// const hostAddress = (isDevelopment)? '0.0.0.0' : '127.0.0.1'


const CWD = process.cwd() // current working directory, it's entirely dependent on what directory the process was launched from

const databaseUrl = 'mongodb://localhost:27017/minecraft'
const staticsPath = CWD +'/statics/'
const imagesPath = CWD +'/statics/images/'
const logPath = CWD +'/config/'


const IDPAY_API_KEY = '9e314057-90e6-4bfd-add3-12227109da35'
const IDPAY_CREATE_TRANSACTION = 'https://api.idpay.ir/v1.1/payment'
const IDPAY_VERIFY_TRANSACTION = 'https://api.idpay.ir/v1.1/payment/verify'


const NESHAN_API_KEY = 'service.8cFpe7Ogvml92xzKPCWsMioJ4hMPXt95GOuUVjbr'
const MAPIR_API_KEY = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJmMTAwNzgxMjZkNWM5ZjZjM2U2OWU3YWMzZDAzNmUzOGQ0Y2M5NDgwMGIxNmVmYzExMWU1MzNkNTdiNjlkNTlhMGRlMjg2NDUxYjI4MzMxIn0.eyJhdWQiOiJteWF3ZXNvbWVhcHAiLCJqdGkiOiJiZjEwMDc4MTI2ZDVjOWY2YzNlNjllN2FjM2QwMzZlMzhkNGNjOTQ4MDBiMTZlZmMxMTFlNTMzZDU3YjY5ZDU5YTBkZTI4NjQ1MWIyODMzMSIsImlhdCI6MTU1ODE2MDc3NCwibmJmIjoxNTU4MTYwNzc0LCJleHAiOjE1NTgxNjQzNzQsInN1YiI6IiIsInNjb3BlcyI6WyJiYXNpYyIsImVtYWlsIl19.pBibHOgBZvyYy6lMVGPNjy9TTPncHu_4YC78DXALPanq1eixIBLFFJBCu0knupXsnjeqFZfC0Jp9VF91GY1dWjoVyPB9akIjlq4qHBXLWPpfsfSVUloSnxkTCrxxe-1tTdwKtUHbK_2JiV3iVbUG_jcLo0BiSariE6oW7_vCvHyTBIm6-x2RCrBpnh_OgznXdQdLPMQMbQfNwArBs6StNodfx5b9NK3GD5Q_iYCEKPOB_jrOnMTy_GD0URUU7ycDkOp0C0_40Xooqw0AV2rROjvL6GV68cjjticELKgsPNnoM0yyCOWQ24YsJH2IuPEs3SYtVZ4P3uGTsN3xwDR6Hg5'
const PUSHE_TOKEN = 'd83a7750d9de4eb43f7650cc86576c1d0fd60ee9'


const KAVE_NEGAR_API_KEY = '4F5542794142316A556F47574765332F77656853393364526E6F3558706D6654686B2B2B34642B75334C593D'
const KAVE_NEGAR_LOOKUP_URL = `https://api.kavenegar.com/v1/${KAVE_NEGAR_API_KEY}/verify/lookup.json`
const KAVE_NEGAR_SEND_URL = `https://api.kavenegar.com/v1/${KAVE_NEGAR_API_KEY}/sms/send.json`



const publicKey = 'BF4D-KmyDcU7oHjSfw3RnphdyQ50MjIuI3ZdRUNasrFQkthZqgwr7I-Z3OuRS2VgyP2Oumx-1PnK_3dsXWDgA9k'
const privateKey = 'Yfzco_OhrbxMB-2R_2PZHSg86pxvATd8ZxVjVTuZhVo'

const vapidDetails = {
    subject: "res:test@test.com",
    publicKey,
    privateKey
}

const adminJwtSecret = 'thesecretforadminbooz'
const customerJwtSecret = 'thesecretforcustomerbooz'

const webPushOptions = {TTL: 12*60*60} // in seconds


class LogWriteStream {

    // constructor(name, fierce){
    //     this._stream = name
    // }
    static getStream() {
        if (this._stream) {
            // console.log('returned Stream')
            return this._stream

        } else {
            // console.log('created Stream')
            // flag: 'a' Open file for appending. The file is created if it does not exist.
            return this._stream = fs.createWriteStream(logPath  + 'logs.txt', {flags:'a'})
        }
    }
}


function log (message, flagParam = '') {

    let signFlag = `${flagParam? '['+flagParam+']' : '' }`

    // no need to write empty lines to log file
    if (message !== '\n')
        LogWriteStream.getStream().write(
            moment().tz("Asia/Tehran").format('YYYY/MM/DD HH:mm:ss')
            + `  ${message}  ${signFlag}\n`)

    // does not need to close cause default option is AutoClose:true when createdWriteStream
    // stream.end()

    // TODO: dont log in real production mode
    // if (isDevelopment)
        console.log(`${message} ${signFlag}`)
    // console.log('\x1b[33m%s\x1b[0m', 'assdfsdgsdf')  //yellow
}


function ensureDirectoryExistence (path, callback) {

    return new Promise(function(resolve, reject) {
        // this is asynchronously
        fs.mkdir(path, { recursive: true }, (err) => {

            if (err) {
                log(err)
                reject()
            }
            else {
                resolve()
                if (callback) callback(err)
                log(`${path} ensured to exist`)
            }
        })
    })
}

function checkFileExistence (path, callback) {

    // Check if the file exists
    fs.access(path, fs.constants.F_OK, (err) => {
        if (callback) callback(err)
    })
}

async function postRequest(url, params, headers) {

    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

    // XMLHttpRequest from a different domain cannot set cookie values for their own domain,
    // unless withCredentials is set to true before making the request.
    const res = await axios.post(url, params, { headers}).catch(err => log(err))

    return {statusCode: res.status, data: res.data}
}

async function getRequestWithParams(url, params) {

    // url must be complete http address
    // var url = new URL(url)

    // console.log(params)

    Object.keys(params).forEach((key, i) => {

        if (i === 0) url += '?'
        url += key + '=' + params[key]

        // if was not last index
        if (i !== Object.keys(params).length - 1) url += '&'
    })

    console.log(url)

    const res = await axios.get(url).catch(err => log(err))

    return {statusCode: res.status, data: res.data}
}

async function getRequest(url, config) {

    const res = await axios.get(url, config).catch(err => log(err))

    return {statusCode: res.status, data: res.data}
}

// NOTE: url must not have http://
async function postToPhp(url, params) {

    return new Promise((resolve, reject) => {

        const http = require('http')

        const querystring = require("querystring")
        const qs = querystring.stringify(params)
        // console.log(qs)

        let hostname
        let path

        for (let i = 0; i < url.length; i++) {
            if (url.charAt(i) === '/') {
                hostname = url.substr(0, i)
                path = url.substring(i, url.length)
                break
            }
        }

        const options = {
            hostname,
            port: 80,
            path,
            method: 'POST',
            headers:{ 'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': qs.length },
            // timeout: 5000,
        }

        let buffer = ""
        const req = http.request(options, function(res) {
            res.on('data', (chunk) => buffer += chunk )
            res.on('end', () => resolve(buffer) )
        })

        req.on('error', function(err) {
            log(err)
        })

        req.write(qs)
        req.end()
    })
}

function replaceAt(index, replacement) {
    return substr(0, index) + replacement + substr(index + replacement.length)
}

function getFormattedPrice (price) {

    if (!price) return price
    price =  price.toString() // converting to String

    return price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function getThousandToman(price) {
    price  = price.toString()
    return price.substr(0, price.length-3)
}

function isDigit(char) {
    return char == '0' || char == '1' || char == '2' || char == '3' || char == '4' || char == '5' || char == '6' || char == '7' || char == '8' || char == '9'
}

function includesLetter(str) {

    for (let i = 0; i < str.length; i++)
        if (isDigit(str.charAt(i)) === false) return true

    return false
}

function monthToString(month) {
    switch (month) {
        case '01': return 'فروردین'
        case '02': return 'اردیبهشت'
        case '03': return 'خرداد'
        case '04': return 'تیر'
        case '05': return 'مرداد'
        case '06': return 'شهریور'
        case '07': return 'مهر'
        case '08': return 'آبان'
        case '09': return 'آذر'
        case '10': return 'دی'
        case '11': return 'بهمن'
        case '12': return 'اسفند'
    }
}

function monthToNumber(number) {
    switch (number) {
        case 'فروردین': return '01'
        case 'اردیبهشت': return '02'
        case 'خرداد': return '03'
        case 'تیر': return '04'
        case 'مرداد': return '05'
        case 'شهریور': return '06'
        case 'مهر': return '07'
        case 'آبان': return '08'
        case 'آذر': return '09'
        case 'دی': return '10'
        case 'بهمن': return '11'
        case 'اسفند': return '12'
    }
}


function degreesToRadians(degrees) {
    return degrees * Math.PI / 180
}

function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
    var earthRadiusKm = 6371

    var dLat = degreesToRadians(lat2-lat1)
    var dLon = degreesToRadians(lon2-lon1)

    lat1 = degreesToRadians(lat1)
    lat2 = degreesToRadians(lat2)

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2)
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
    return earthRadiusKm * c
}

const toType = (obj) => {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

// for saving database
function getDateInString() {

    const now = dConverter.getLiveDate()
    let day = now.substring(8, 10)

    if (day.charAt(0) === '0') day = day.charAt(1)

    // direction issue
    return day + ' ' + monthToString(now.substring(5, 7)) + ' ' + now.substring(0, 4)
}



module.exports = {
    getDateInString,
    staticsPath,
    isDevelopment,
    CWD,
    // baseAddress,
    // productionAddress,
    // localNetworkAddresses,
    // managerPanelAddress,

    IDPAY_API_KEY,
    IDPAY_CREATE_TRANSACTION,
    IDPAY_VERIFY_TRANSACTION,
    hostAddress,
    databaseUrl,
    imagesPath,
    log,
    logPath,
    ensureDirectoryExistence,
    checkFileExistence,
    publicKey,
    privateKey,
    adminJwtSecret,
    customerJwtSecret,
    postRequest,
    getRequest,
    getRequestWithParams,
    replaceAt,
    NESHAN_API_KEY,
    MAPIR_API_KEY,
    webPushOptions,
    PUSHE_TOKEN,
    isDigit,
    monthToString,
    getThousandToman,
    includesLetter,
    postToPhp,
    distanceInKmBetweenEarthCoordinates,
    monthToNumber,
    KAVE_NEGAR_API_KEY,
    KAVE_NEGAR_LOOKUP_URL,
    vapidDetails,
    toType,
    getFormattedPrice,

    // Execute shell commands (Recursive Function)
    executeCommands: function(commands, index, callback) {

        if (!commands[index]) return console.log('no commands')

        exec(commands[index], (error, stdout, stderr) => {

            // console.log('index = ' + index)
            if (error) console.log(`error: ${error.message}`)
            if (stderr) console.log(`stderr: ${stderr}`)
            if (stdout) console.log(`stdout: ${stdout}`)

            index++

            // if there was more commands
            if (commands[index])
                executeCommands(commands, index, callback)
            else
                if (callback) callback()
        })
    },

    getRandomInteger : function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
