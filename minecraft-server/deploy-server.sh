#!/bin/bash

# handle server stuff
# compress zip file
tar czf minecraft-server.tar.gz index.js package.json

# it copies file to mohamad user home(~) directory
# if wanna go to root's home root@78.46.12.130:~
scp -P 2020 minecraft-server.tar.gz 185.94.99.199:~


ssh 185.94.99.199 -p 2020 << 'ENDSSH'

pm2 stop minecraft-server




rm -rf minecraft-server
mkdir minecraft-server

# extract file  -c means in minecraft-server directory
tar xf minecraft-server.tar.gz -C minecraft-server
rm minecraft-server.tar.gz


cd minecraft-server
npm install





#pm2 start minecraft-server
ENDSSH
