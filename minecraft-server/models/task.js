const mongoose = require('mongoose')

const consts = require("../utils/consts")
const dConverter = require('../tools/dateConverter')

// Using mongo for temp info because we want all node processes to access common temp info (in case running multiple nodes)
// And don't need in-memory data stores like redis and memcached yet
const schema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,

    initializingServersCount: {
        type: Number,
        default: 0
    },
    runningWorldsCount: {
        type: Number,
        default: 0
    },

    created: {
        type: String,
        default: () => {
            return dConverter.getLiveDate()
        }
    },

})

module.exports = mongoose.model(consts.TEMP_STATIC, schema)
