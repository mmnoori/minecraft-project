const mongoose = require('mongoose')

const consts = require ("../utils/consts")
const dConverter = require('../tools/dateConverter')

const schema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true,
        dropDups: true,
        trim: true },
    version: { type: String, trim: true },

    port:{
        type: Number,
        required: true,
        unique: true,
        dropDups: true,
    },
    owner: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: consts.USER
        }
    ],
    banned: {
        type: Boolean,
        default: false
    },
    created: {
        type: String,
        default: () => {
            return dConverter.getLiveDate()
        }
    },


})

module.exports = mongoose.model(consts.WORLD, schema)
