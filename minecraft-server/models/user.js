const mongoose = require('mongoose')

const consts = require ("../utils/consts")
const dConverter = require('../tools/dateConverter')

const customerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    code:{
        type: String,
        required: true,
        unique: true,
        dropDups: true,
    },
    username: {
        type: String,
        trim: true,
        required: true,
        unique: true,
        dropDups: true,
    },
    password: { type: String, trim: true },
    phone: { // defining as String because of 0 start issue
        type: String,
        required: true,
        unique: true,
        dropDups: true,
        trim: true
    },
    credit: {
        type: Number,
        default: 0
    },
    inviteds: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: consts.CUSTOMER
        }
    ],
    // cause we need to sort by invitedsCount
    // we can use mongoDB aggregation but its definitely not efficient for processing like 5 thousands of customers
    // and then sorting them
    invitedsCount: {
        type: Number,
        default: 0
    },
    inviter: {
        type: mongoose.Schema.Types.ObjectId,
        ref: consts.CUSTOMER
    },
    worlds: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: consts.WORLD
        }
    ],
    // number of customer's delievered/successful orders
    // cause we need to sort by ordersCount
    worldsCount: {
        type: Number,
        default: 0
    },
    usedDiscounts:{ // array of discountCode codes
        type: Array,
        default: []
    },
    webSubscription: {
        type: mongoose.Schema.Types.ObjectId,
        ref: consts.SUBSCRIPTION
    },
    banned: {
        type: Boolean,
        default: false
    },
    created: {
        type: String,
        default: () => {
            return dConverter.getLiveDate()
        }
    },


})

// module.exports.customerSchema = customerSchema

// if you export a model as shown below, the model will be scoped
// to Mongoose's default connection.
module.exports = mongoose.model(consts.USER, customerSchema)
